SELECT 
    count(http.statusCode)
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}}) 
FACET http.statusCode