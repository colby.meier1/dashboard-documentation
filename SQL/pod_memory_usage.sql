SELECT 
    latest(memoryUsedBytes/memoryLimitBytes) * 100 as 'Pod Memory Usage' 
FROM K8sContainerSample 
FACET podName 
TIMESERIES 1 minute 
SLIDE BY 10 minutes