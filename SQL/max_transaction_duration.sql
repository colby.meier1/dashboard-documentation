SELECT 
    max(duration * 1000) as Maximum 
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})  
LIMIT MAX
TIMESERIES 10 seconds 
SLIDE BY 30 seconds