SELECT 
    max(duration) AS 'Max Duration', 
    average(duration) as 'Avg Duration', 
    latest(duration) as 'Current Duration', 
    latest(databaseDuration) as 'DB Duration', 
    latest(externalDuration) as 'External Duration', 
    count(*) AS 'Total Calls' 
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}}) 
FACET name 
SINCE 1 hours ago 
LIMIT MAX 
ORDER BY 'Max Duration' DESC