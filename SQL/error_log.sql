SELECT 
      duration,
      externalDuration,
      http.statusCode,
      request.headers.host,
      request.headers.userAgent,
      request.method,
      request.uri,
      transactionName,
      trace.id, appName 
    FROM
        Log 
        JOIN (
            FROM 
                TransactionError 
            WITH 
                traceId 
            AS 
                trace.id 
            SELECT 
                trace.id, 
                duration, 
                externalDuration, 
                http.statusCode, 
                request.headers.host, 
                request.headers.userAgent, 
                request.method, 
                request.uri, 
                transactionName, appName  
            WHERE 
                appName = concat({{ENV}}, ' UMS ', {{APP}})
        ) 
    ON 
        trace.id  
    WHERE 
        (level = 'FATAL' OR level = 'ERROR')