SELECT 
    count(*) as Connections 
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}}) 
FACET substring(`request.headers.userAgent`, 0, 50) as 'User Agent' 
limit 10 
COMPARE WITH 1 hour ago