SELECT 
    count(traceId) as `Last Week` 
FROM Transaction 
SINCE 1 hour ago 
COMPARE WITH 1 week ago 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})