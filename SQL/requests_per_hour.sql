SELECT 
    rate(count(*), 1 hour) as 'Requests Per Hour' 
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})  
TIMESERIES 1 minute 
SLIDE BY 5 minutes 
LIMIT MAX