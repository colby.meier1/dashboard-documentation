SELECT 
    rate(count(*), 1 minute) as 'Requests Per Minute' 
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})  
TIMESERIES 
LIMIT 10 
FACET `request.headers.userAgent`