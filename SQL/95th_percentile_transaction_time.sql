SELECT 
    percentile(duration, 95) * 1000 as '95th Percentile' 
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})  
LIMIT MAX 
TIMESERIES