SELECT 
    fullHostname, 
    net.errorsPerSecond, 
    hostStatus 
FROM K8sPodSample 
WHERE net.errorsPerSecond > 0