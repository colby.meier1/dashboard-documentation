SELECT 
    percentile(duration, 95) * 1000 as '95th Percentile', 
    median(duration * 1000) as Median, 
    average(duration * 1000) as Average, 
    max(duration * 1000) as Max 
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})  
LIMIT MAX 
TIMESERIES 10 seconds 
SLIDE BY  30 seconds  
SINCE 15 minutes ago 