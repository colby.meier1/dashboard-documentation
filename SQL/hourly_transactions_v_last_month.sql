SELECT 
    count(traceId) as `Last Month` 
FROM Transaction 
SINCE 1 hour ago 
COMPARE WITH 1 day ago 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})