SELECT 
    median(duration * 1000) as Median 
FROM Transaction 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})  
LIMIT MAX 
TIMESERIES