SELECT 
    count(traceId) as `Yesterday`
FROM Transaction 
SINCE 1 hour ago
COMPARE WITH 1 day ago
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})