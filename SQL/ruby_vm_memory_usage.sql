SELECT 
    ((average(apm.service.memory.physical) * rate(count(apm.service.instance.count), 1 minute))*1000*1000) 
FROM Metric 
FACET `host.displayName` 
TIMESERIES 
WHERE appName = concat({{ENV}}, ' UMS ', {{APP}})