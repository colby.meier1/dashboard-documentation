SELECT 
    latest(cpuUsedCores/cpuLimitCores) * 100 as 'Pod CPU Usage' 
FROM K8sContainerSample 
FACET podName 
TIMESERIES 1 minute